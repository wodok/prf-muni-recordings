from datetime import datetime, timedelta
import os
import logging

LECTURES = None
LECTURES_test1 = [
    ["https://cdn.streaming.cesnet.cz/muni/law136.stream/chunklist.m3u8", "Test1: Test1", "testroom", "Probíhá test", datetime.now() - timedelta(hours=0, minutes=2), datetime.now() + timedelta(hours=1, minutes=28)],
    ["https://cdn.streaming.cesnet.cz/muni/law140.stream/chunklist.m3u8", "Test2: Test2", "testroom", "Probíhá test", datetime.now(), datetime.now() + timedelta(hours=1, minutes=40)],
    ["https://cdn.streaming.cesnet.cz/muni/law025.stream/chunklist.m3u8", "Test3: Test3", "testroom", "Probíhá test", datetime.now() + timedelta(hours=0, minutes=2), datetime.now() + timedelta(hours=1, minutes=47)]
]
LECTURES_test2 = [
    ["https://cph-p2p-msl.akamaized.net/hls/live/2000341/test/level_0.m3u8", "Test1: Test1", "testroom", "Probíhá test", datetime.now() - timedelta(hours=0, minutes=2), datetime.now() + timedelta(hours=0, minutes=3)],
    ["https://cph-p2p-msl.akamaized.net/hls/live/2000341/test/level_1.m3u8", "Test2: Test2", "testroom", "Probíhá test", datetime.now(), datetime.now() + timedelta(hours=0, minutes=5)],
    ["https://cph-p2p-msl.akamaized.net/hls/live/2000341/test/level_2.m3u8", "Test3: Test3", "testroom", "Probíhá test", datetime.now() + timedelta(hours=0, minutes=2), datetime.now() + timedelta(hours=0, minutes=7)]
]

ICAL = os.path.join(os.path.dirname(os.path.abspath(__file__)), "schedule.ics")

get_uri = lambda location: f"https://cdn.streaming.cesnet.cz/muni/law{location.strip()}.stream/chunklist.m3u8"

AUTOUPLOAD_FB = False
AUTOUPLOAD_ONEDRIVE = False
REMOVE_ORIG = False

DOWNLOAD_FOLDER = "C:\\Users\\<user>\\Documents\\sandbox\\recordings"
ONEDRIVE_FOLDER = None
STREAM_CLASSES = ["025", "030", "034", "038", "133", "136", "140"]
WORKSTATION = "DE1"
FB_URI = "https://www.facebook.com/groups/289719872967553"

LOGGING = logging.DEBUG