import os
import shutil
import subprocess
import tempfile
import sys
import logging
        
def process_to_mp4(input_file, output_file):

    with tempfile.TemporaryDirectory() as temp_location:

        # Create temporary files for the input and output audio tracks.
        temp_mp4 = os.path.join(temp_location, 'video.mp4')
        # temp_input_wav = os.path.join(temp_location, 'track_in.wav')
        # temp_output_wav = os.path.join(temp_location, 'track_out.wav')
        # temp_output_aac = os.path.join(temp_location, 'track_out.aac')

        # Make MP4
        subprocess.run([
            'ffmpeg', 
            '-loglevel', 'warning',
            '-hide_banner',
            '-i', input_file,
            '-c', 'copy',
            temp_mp4
            ])
        
        subprocess.run([
            'ffmpeg',
            '-loglevel', 'warning',
            '-hide_banner',
            '-i', temp_mp4,
            '-filter_complex', '[0:a]pan=1c|c0=c0[left];[0:a]amerge=inputs=1[aout]',
            '-map', '[left]',
            '-map', '[aout]',
            '-map', '0:v',
            '-c:v', 'copy',
            '-metadata:s:a:0', 'language=ces',
            '-metadata:s:a:0', 'title=mono left',
            '-disposition:a:0', 'default',
            '-metadata:s:a:1', 'language=ces',
            '-metadata:s:a:1', 'title=original',
            output_file
        ])
        
        # # Extract the audio track from the input video file.
        # subprocess.run([
        #     'ffmpeg', '-loglevel', 'warning', '-hide_banner', '-i', temp_mp4, '-vn', temp_input_wav
        #     ])

        # try:
        #     logging.info('Audio denoise start')
        #     # Denoise the audio track.
        #     process_audio(temp_input_wav, output_file=temp_output_wav, window_size=0)
        #     logging.info('Audio denoise end')
        #     # Encode the denoised audio track to AAC.
        #     subprocess.run([
        #         'ffmpeg', '-loglevel', 'warning', '-hide_banner', '-i', temp_output_wav, '-codec:a', 'aac', temp_output_aac
        #         ])

        #     # Combine the denoised audio track with the original video file.
        #     subprocess.run([
        #         'ffmpeg', '-loglevel', 'warning', '-hide_banner', '-i', temp_mp4, '-i', temp_output_aac, '-map', '0:v:0', '-map', '1:a:0', '-map', '0:a:0', '-c', 'copy', '-metadata:s:a:0', 'language=cze', '-metadata:s:a:0', 'title=filtered (logmmse)', '-disposition:a:0', 'default', '-metadata:s:a:1', 'language=cze', '-metadata:s:a:1', 'title=original', output_file
        #         ])
        # except Exception as e:
        #     logging.error(e, exc_info=True)
        #     shutil.copy(temp_mp4, output_file)

def reduce_mp4(file_in, file_out):
    subprocess.run([
            'ffmpeg', '-loglevel', 'warning', '-hide_banner', '-i', file_in, file_out
            ])

if __name__ == '__main__':
    logging.basicConfig(
        level=logging.DEBUG,
        format='%(asctime)s [%(levelname)s]: %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S',
        handlers=[
            logging.StreamHandler()
        ]
    )
    input_file = sys.argv[1]
    output_file = "output.mp4"

    # Add the denoised audio track to the video file.
    process_to_mp4(input_file, output_file)