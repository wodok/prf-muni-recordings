import sys
import config
import os
import platform
import shutil
import subprocess


def upload_onedrive(file):
    basename = os.path.basename(file)
    out_file = os.path.join(config.ONEDRIVE_FOLDER, basename)
    shutil.copyfile(file, out_file)
    if platform.system() == "Linux":
        subprocess.run(['onedrive', '--synchronize', '--upload-only', '--no-remote-delete'])
    else:
        print('Onedrive not implemented for Windows')
    os.remove(out_file)


if __name__ == "__main__":	
	upload_onedrive(sys.argv[1])