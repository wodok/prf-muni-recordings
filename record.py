from enum import Enum
import subprocess
import sys
from datetime import datetime, timedelta, timezone
import time
import os
import multiprocessing as mp
from typing import List
import icalendar
import config
import logging
from downloader import hls_fetch
from logging.handlers import RotatingFileHandler
from upload_fb import upload_fb
from upload_onedrive import upload_onedrive
from dateutil import rrule
from video_processor import process_to_mp4, reduce_mp4

# Configure the logging
log_filename = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'error_log.txt')
logging.basicConfig(
    level=config.LOGGING,
    format='%(asctime)s [%(levelname)s]: %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S',
    handlers=[
        RotatingFileHandler(log_filename, mode='a', maxBytes=20*1024*1024, 
                                 backupCount=2, encoding=None, delay=0),
        logging.StreamHandler()
    ]
)

processing_lock = False

class Lecture:   
    class State(Enum):
        WAITING = 0,
        RECORDING = 2,
        
        PROCESSING = 10,
        CONCATENATE = 11,
        TS_TO_MP4 = 12,
        UPLOAD_FB = 14,
        REMOVE_TS = 15,
        
        REDUCE_MP4 = 21,
        REMOVE_ORIG_MP4 = 22, 
        UPLOAD_ONEDRIVE = 23,

        PROCESSING_END = 90,

        DONE = 99

    def __init__(self, stream, name, location, description, starttime, endtime):
        self.real_start = None
        self.output_file = None
        self.output_red_file = None
        self.parts = [] 
        self.process = None
        self.stream = stream
        self.name = name
        self.location = location
        self.description = description
        self.starttime = starttime
        self.endtime = endtime
        self.lecture = False

        self.state = Lecture.State.WAITING

        if self.get_end() < datetime.now():
            self.state = Lecture.State.DONE
        
        try:
            self.code = name.split(":")[0].replace("/", "-")
            if not "-" in self.code:
                self.lecture = True
        except:
            self.code = "UNDEFINED_ERROR"
    
    def __del__(self):
        if self.process is not None:
            logging.error("Releasing process in destructor "+self.to_str())
            self.process.kill()
            self.process = None

    def get_end(self):
        return self.endtime
    
    def get_start(self):
        return self.starttime

    def should_be_recording(self):
        now = datetime.now()
        return (now > self.get_start()) and (now < self.get_end())
    
    def get_state(self):
        if self.state == Lecture.State.WAITING and self.should_be_recording():
            self.state = Lecture.State.RECORDING
        return self.state

    def loop(self):
        global processing_lock
        if self.state == Lecture.State.RECORDING:
            if self.real_start is None:
                    self.real_start = datetime.now()
                    self.output_file = os.path.join(config.DOWNLOAD_FOLDER, f'{self.real_start.strftime("%y-%m-%d_%H%M")}_{self.code}_{config.WORKSTATION}.mp4')
            if self.should_be_recording():
                if self.process is not None and self.process.exitcode is not None:
                    self.process.close()
                    self.process = None
                    if (not os.path.exists(self.parts[-1])):
                        self.parts.pop()
                if self.process is None:
                    logging.info(f"New part #{len(self.parts)} - " + self.to_str())
                    self.parts.append(os.path.join(config.DOWNLOAD_FOLDER, f'{self.real_start.strftime("%y-%m-%d_%H%M")}_{self.code}_({len(self.parts)})_{config.WORKSTATION}.ts'))
                    self.process = mp.Process(target = hls_fetch, args=(self.stream, self.parts[-1], self.endtime))
                    self.process.start()
            else:                 
                if self.process.exitcode is not None:
                    self.process.close()
                    self.process = None
                    if (not os.path.exists(self.parts[-1])):
                        self.parts.pop()
                    logging.info('Recording closed ' + self.to_str())
                    self.state = Lecture.State.PROCESSING
                if (len(self.parts) == 0):
                    logging.warning('Nothing recorded ' + self.to_str())
                    self.state = Lecture.State.DONE
        else:
            if self.state == Lecture.State.PROCESSING:
                if not processing_lock:
                    processing_lock = True
                    logging.info('Processing start ' + self.to_str())
                    self.state = Lecture.State.CONCATENATE

            if self.state == Lecture.State.CONCATENATE:
                if len(self.parts) > 1:
                    if self.process is None:
                        self.process = mp.Process(target = downloader.concatenate_files, args=(self.parts[1:], self.parts[0]))
                        self.process.start()
                        logging.debug('Concateting parts ' + self.to_str())
                    elif self.process.exitcode is not None:
                        self.process.close()
                        self.process = None
                        self.state = Lecture.State.TS_TO_MP4
                else:
                    self.state = Lecture.State.TS_TO_MP4

            if self.state == Lecture.State.TS_TO_MP4:
                if self.process is None:
                    self.process = mp.Process(target = process_to_mp4, args=(self.parts[0], self.output_file))
                    self.process.start()
                    logging.debug('Converting to MP4 ' + self.to_str())
                elif self.process.exitcode is not None:
                        self.process.close()
                        self.process = None
                        self.state = Lecture.State.UPLOAD_FB

            if self.state == Lecture.State.UPLOAD_FB:
                if config.AUTOUPLOAD_FB and self.lecture:
                    if self.process is None:
                        self.process = mp.Process(target = upload_fb, args=(self.output_file, f"{self.name} - {self.starttime.strftime('%d.%m.%y %H:%M')} [{config.WORKSTATION}]\nLocation: {self.location}"))
                        self.process.start()
                        logging.debug('Uploading FB' + self.to_str())
                    elif self.process.exitcode is not None:
                        self.process.close()
                        self.process = None
                        self.state = Lecture.State.REMOVE_TS
                else:
                    self.state = Lecture.State.REMOVE_TS

            if self.state == Lecture.State.REMOVE_TS:
                if self.process is None:
                        if not (os.path.exists(self.output_file)):
                            logging.error('Output file unavailable ' + self.to_str())
                            self.state = Lecture.State.PROCESSING_END
                        elif (os.path.getsize(self.output_file) > 0.8 * os.path.getsize(self.parts[0])):
                            self.process = mp.Process(target = remove_files, args=(self.parts,))
                            self.process.start()
                            logging.debug('Removing TS ' + self.to_str())
                        else:
                            logging.warning('Size check failed ' + self.to_str())
                            self.state = Lecture.State.REDUCE_MP4
                elif self.process.exitcode is not None:
                        self.process.close()
                        self.process = None
                        self.state = Lecture.State.REDUCE_MP4          
            
            if self.state == Lecture.State.REDUCE_MP4:
                if self.process is None:
                    self.output_red_file = os.path.join(config.DOWNLOAD_FOLDER, f'{self.real_start.strftime("%y-%m-%d_%H%M")}_{self.code}_{config.WORKSTATION}_red.mp4')
                    self.process = mp.Process(target = reduce_mp4, args=(self.output_file, self.output_red_file))
                    self.process.start()
                    logging.debug('Reducing MP4 ' + self.to_str())
                elif self.process.exitcode is not None:
                        self.process.close()
                        self.process = None
                        self.state = Lecture.State.REMOVE_ORIG_MP4
            
            if self.state == Lecture.State.REMOVE_ORIG_MP4:
                if config.REMOVE_ORIG:
                    if self.process is None:
                            if not (os.path.exists(self.output_red_file)):
                                logging.error('Reduced output file unavailable ' + self.to_str())
                                self.state = Lecture.State.PROCESSING_END
                            elif (os.path.getsize(self.output_red_file) > 0.1 * os.path.getsize(self.output_file)):
                                self.process = mp.Process(target = remove_files, args=([self.output_file],))
                                self.process.start()
                                self.output_file = None
                                logging.debug('Removing original MP4 ' + self.to_str())
                            else:
                                logging.warning('Reduced size check failed ' + self.to_str())
                                self.state = Lecture.State.PROCESSING_END
                    elif self.process.exitcode is not None:
                            self.process.close()
                            self.process = None
                            self.state = Lecture.State.UPLOAD_ONEDRIVE
                else:
                    self.state = Lecture.State.UPLOAD_ONEDRIVE 
            
            if self.state == Lecture.State.UPLOAD_ONEDRIVE:
                if config.AUTOUPLOAD_ONEDRIVE:
                    if self.process is None:
                        self.process = mp.Process(target = upload_onedrive, args=(self.output_red_file,))
                        self.process.start()
                        logging.debug('Uploading ONEDRIVE' + self.to_str())
                    elif self.process.exitcode is not None:
                        self.process.close()
                        self.process = None
                        self.state = Lecture.State.PROCESSING_END
                else:
                    self.state = Lecture.State.PROCESSING_END          

            if self.state == Lecture.State.PROCESSING_END:
                logging.info('Processing end ' + self.to_str())
                processing_lock = False
                self.state = Lecture.State.DONE

    def to_str(self):
        return f"{self.code} {'(FB) ' if self.lecture else ''}[{self.location}] {self.starttime.strftime('%y-%m-%d %H:%M')} - {self.endtime.strftime('%y-%m-%d %H:%M')}"

def remove_files(file_paths):
  for file_path in file_paths:
    os.remove(file_path)

def main() -> int:
    while(True):
        try:
            requests = get_requests()
            logging.info("Requests loaded. Runnning.")
            while(True):
                index_done = next((i for i, item in enumerate(requests) if item.get_state() != Lecture.State.DONE), None)
                if index_done is None:
                    break
                elif index_done > 0:
                    requests = requests[index_done:]
                
                index_running = next((i for i, item in enumerate(requests) if item.get_state() == Lecture.State.WAITING), None)
                if index_running is None or index_running > 0:
                    for request in requests[0:index_running]:
                        request.loop()
                    time.sleep(5)
                else:
                    sleep_time = (requests[0].get_start() - datetime.now()).total_seconds()           
                    if sleep_time>0:
                        if sleep_time>3600:
                            sleep_time = 3600
                        time.sleep(sleep_time)
            logging.info('FINISHED ALL REQUESTS - DONE!')
            while(True):
                time.sleep(3600)
            break
        except KeyboardInterrupt:
            logging.warning("Ctrl+C pressed. Exiting gracefully.")
            break
        except Exception as e:
            logging.error(e, exc_info=True)  # Log the error with traceback
            time.sleep(60)
            processing_lock = False

def get_requests():
    if config.LECTURES is not None:
        return [Lecture(*lecture) for lecture in config.LECTURES]
    else:
        lectures: List[Lecture] = []
        with open(config.ICAL, 'r', encoding='utf-8') as f:
            cal = icalendar.Calendar.from_ical(f.read())
            for item in cal.subcomponents:
                if item.name == 'VEVENT' and any(stream_class in (item["location"]).strip() for stream_class in config.STREAM_CLASSES):
                        starttime = item['dtstart'].dt.replace(tzinfo=None)
                        duration = item['duration'].dt

                        try:
                            code = item["summary"].split(":")[0].replace("/", "-")
                            if "-" in code: # Seminars are skipped
                                continue
                        except:
                            continue

                        if 'RRULE' in item:
                            rule1: icalendar.vRecur = item['RRULE']
                            rule = rrule.rrulestr(rule1.to_ical().decode('utf-8'), dtstart=starttime.replace(tzinfo=timezone.utc))
                            for occurrence in rule.between((datetime.now() - duration).replace(tzinfo=timezone.utc), datetime.max.replace(tzinfo=timezone.utc)):
                                new_lecture = Lecture(config.get_uri(item['location']), item["summary"],  item['location'], item['description'] if 'description' in item else '', occurrence.replace(tzinfo=None), occurrence.replace(tzinfo=None)+duration+timedelta(minutes=5))
                                lectures.append(new_lecture)
                        else:
                            if (starttime + duration) > datetime.now():
                                new_lecture = Lecture(config.get_uri(item['location']), item["summary"],  item['location'], item['description'] if 'description' in item else '', starttime, starttime+duration+timedelta(minutes=5))
                                lectures.append(new_lecture)
        lectures.sort(key= lambda lecture: lecture.get_start())
        for lecture in lectures:
            logging.info('INCLUDE ' + lecture.to_str())
        return lectures

if __name__ == '__main__':
    sys.exit(main())
