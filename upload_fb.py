import sys
import config
import os
import platform
import subprocess
from unidecode import unidecode

fb_autopost = os.path.join(os.path.dirname(os.path.abspath(__file__)), "fb-autopost", "fb_autopost.py")

def upload_fb(file, caption):
    global fb_autopost
    if platform.system() == "Linux":
        subprocess.run(['xvfb-run', '-a', 'python3', fb_autopost, unidecode(caption),  file, config.FB_URI])
    else:
        subprocess.run(['python', fb_autopost, unidecode(caption),  file, config.FB_URI])

if __name__ == "__main__":
	upload_fb(sys.argv[1], sys.argv[2])